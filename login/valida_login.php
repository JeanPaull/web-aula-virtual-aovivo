<?php  
    include_once '../includes/configuracoes_site.php';//Configuração conexão com banco de dados PDO 
    session_start();//INICIA SESSÃO
    if($_SESSION):
            $_SESSION['email'] =  '';
            $_SESSION['senha'] =  '';
            unset($_SESSION['email']); 
            unset($_SESSION['senha']); 
           // var_dump($_SESSION);
            unset($_COOKIE);
           //session_destroy();
    endif;
    
    //print_r($_SESSION);
   
    //Libary PagSeguro 
    //include_once '../pagseguro/source/PagSeguroLibrary/PagSeguroLibrary.php';
    
    if (isset($_POST['acao']) && ($_POST['acao'] == 'Logar')){
            
    $erro  		= '';
    $email 		= trim(strip_tags($_POST['email']));
    $senha 		= trim(strip_tags($_POST['senha']));
    $_SESSION['email']  =  $email;
    $_SESSION['senha']  =  $senha;


    if (empty($email)){
            $erro = "<strong >Atenção!</strong> Por favor <b>".$_SESSION['nome']."</b> preencha o campo e-mail";
            $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }
    elseif (empty($senha)){
            $erro = "<strong >Atenção!</strong> Por favor <b>".$_SESSION['nome']."</b> preencha o campo senha";
            $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }	
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $erro = "<strong >Atenção!</strong> Opss... <b>".$_SESSION['nome']."</b> este e-mail ( {$email} ) é inválido! <br /> Por favor insira um e-mail válido. <br />Exemplo: fulano@seudominio.com.br";
             $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }
    else{
            //mata sessão erro se validar o e-mail 
            $_SESSION['erro_login'] =  '';
            unset($_SESSION['erro_login']); 
         
      
            
//  SELECIONA AS MARCAS
//  $stmt = "SELECT * FROM Cliente";
//  $resultado = $conn_pdo->query($stmt);

//  Imprimir os resultados
//  while($row = $resultado->fetch()) {
//         echo $row['id']. ' - '. $row['nome'] . ' - ' . $row['marca'] . '';
//         echo '<option class="info_veiculo_modelo" value="'.$row['id'].'">'.$row['nome'].'</option>';
//      
//  }
                               
                           
            $query_pega_user = $conn_pdo->prepare("SELECT * FROM Cliente WHERE email = '".$_SESSION['email']."'");
            $query_pega_user->execute();        
            $qdUser = $query_pega_user->rowCount(); 
                 
           // echo $qdUser;
            
            if ($qdUser == 1){
                    $query_valida_senha = $conn_pdo->prepare("SELECT * FROM Cliente WHERE email = '".$_SESSION['email']."' AND senha = '".$_SESSION['senha']."'");
                    $query_valida_senha->execute();        
                    $qdValSenha = $query_valida_senha->rowCount(); 
            }

            if ($qdUser == 0):
                    $erro = 'Atenção! <strong>( '.$email.' )</strong>'.'<br /> Você não possui um cadastro!  ';
                            $_SESSION['email'] =  '';
                            $_SESSION['senha'] =  '';
                            unset($_SESSION['email']); 
                            unset($_SESSION['senha']); 
                            session_destroy();
                            session_start();
                            $_SESSION['erro_login'] =  $erro;
                            header("Location: ../cadastro.php"); 
                            echo "<script>history.go(-1)</script>";       
            endif;

    
            
            if (($qdUser == 1) && ($qdValSenha == 0)):
                    $erro = '<strong>Senha incorreta:</strong> a senha que você digitou está incorreta. Tente novamente (tenha certeza de que a função Caps Lock está desligada). <br /><br />Ou entre em contato com a administração relatando o problema através do e-mail:  
                    <strong>industriadavoz@yahoo.com.br</strong> ou pelo WhatsApp: (61) 9624-7771';
                            $_SESSION['email'] =  '';
                            $_SESSION['senha'] =  '';
                            unset($_SESSION['email']); 
                            unset($_SESSION['senha']); 
                            session_destroy();
                            session_start();
                            $_SESSION['erro_login'] =  $erro;
                            echo "<script>history.go(-1)</script>";   
            endif;


            if (($qdUser == 1) && ($qdValSenha == 1)):
                
                   while($row = $query_valida_senha->fetch()) {
                        $ativo = $row['ativo'];
                        $nome  = strtoupper($row['nome']);
                    }
                   
                   if(($ativo) == 1){
                    header("Location: ../saladeaula.php");
                    $erro = 'Olá, seja bem vindo - Logado com sucesso!';
                   }else{
                            $erro = 'Atenção: <strong>'.$nome.', </strong><br />
                                Caso não tenha efetuado o pagamento, por favor <br /><a href="pagamento.php?pagamento=2">Clique Aqui</a>.<br /><br />
                                Caso o pagamento já tenha sido efetuado, por favor entre em contato com a administração relatando o problema através do e-mail:  
                            <strong>industriadavoz@yahoo.com.br</strong> ou pelo WhatsApp: (61) 9624-7771';
                            $_SESSION['email'] =  '';
                            $_SESSION['senha'] =  '';
                            unset($_SESSION['email']); 
                            unset($_SESSION['senha']); 
                            session_destroy();
                            session_start();
                            $_SESSION['erro_login'] =  $erro;
                            echo "<script>history.go(-1)</script>";   
                   }
                   
            
                    
            endif;


            if ($qdUser > 1):
                $erro = 'Entre em contato com a diretoria <strong>Web Aula Virtual</strong>, pois existe 2 usuários com mesmo e-mail cadastrado ';
                $_SESSION['email'] =  '';
                $_SESSION['senha'] =  '';
                unset($_SESSION['email']); 
                unset($_SESSION['senha']); 
                session_destroy();
                session_start();
                $_SESSION['erro_login'] =  $erro;
                echo "<script>history.go(-1)</script>"; 
            endif;	

            }
    //var_dump($_SESSION);

}//VERIFICA CAMPOS EMAIL SENHA EM BRANCO

/*****************************************************************************************************************************************/

 if (isset($_POST['acao']) && ($_POST['acao'] == 'Cadastre-se')){
 
    $erro  		= '';
    $email 		= trim(strip_tags($_POST['email']));
    $nome 		= trim(strip_tags($_POST['nome']));
    $senha 		= trim(strip_tags($_POST['senha']));
    $_SESSION['email']  =  $email;
    $_SESSION['senha']  =  $senha;
    $palestra_aovivo    = trim(strip_tags($_POST['$palestra_aovivo']));
    


    if (empty($email)){
            $erro = "<strong >Atenção!</strong> Por favor <b>".$_SESSION['nome']."</b> preencha o campo e-mail";
            $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }
    elseif (empty($senha)){
            $erro = "<strong >Atenção!</strong> Por favor <b>".$_SESSION['nome']."</b> preencha o campo senha";
            $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }	
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
             unset($_SESSION['cadastro_sucesso']);
             $erro = "<strong >Atenção!</strong> Opss... <b>".$_SESSION['nome']."</b> este e-mail ( {$email} ) é inválido! <br /> Por favor insira um e-mail válido. <br />Exemplo: fulano@seudominio.com.br";
             $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>";
            }
    else{
            //mata sessão erro se validar o e-mail 
            $_SESSION['erro_login'] =  '';
            unset($_SESSION['erro_login']); 
         
      
            
//  SELECIONA AS MARCAS
//    $stmt = "SELECT * FROM Cliente";
//    $resultado = $conn_pdo->query($stmt);

//  Imprimir os resultados
//    while($row = $resultado->fetch()) {
//         echo $row['id']. ' - '. $row['nome'] . ' - ' . $row['marca'] . '';
//         echo '<option class="info_veiculo_modelo" value="'.$row['id'].'">'.$row['nome'].'</option>';
//      
//    }
                               
                           
            $query_pega_user = $conn_pdo->prepare("SELECT * FROM Cliente WHERE email = '".$_SESSION['email']."'");
            $query_pega_user->execute();        
            $qdUser = $query_pega_user->rowCount(); 
              
            if ($qdUser == 1){
                    $query_valida_senha = $conn_pdo->prepare("SELECT * FROM Cliente WHERE email = '".$_SESSION['email']."' AND senha = '".$_SESSION['senha']."'");
                    $query_valida_senha->execute();        
                    $qdValSenha = $query_valida_senha->rowCount(); 
                            
                            $erro = 'Você já possuiu um cadastro, faça o login ou entre em contato com a diretoria <strong>industriadavoz@yahoo.com.br</strong> ou pelo WhatsApp: (61) 9624-7771<br />  ';
                            $_SESSION['email'] =  '';
                            $_SESSION['senha'] =  '';
                            unset($_SESSION['email']); 
                            unset($_SESSION['senha']);
                            unset($_SESSION['cadastro_sucesso']);
                            session_destroy();
                            session_start();
                            $_SESSION['erro_login'] =  $erro;
                            echo "<script>history.go(-1)</script>"; 
                    
            }

            if ($qdUser == 0):
                $stmt = $conn_pdo->prepare("INSERT INTO Cliente (nome, email, senha) VALUES (:nome, :email, :senha)");
                $stmt->bindParam(':nome', $nome);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':senha', $senha);
                $stmt->execute();
                if($stmt){
                        /*$paymentRequest = new PagSeguroPaymentRequest();  
                        $paymentRequest->addItem('0001', 'Legislação Sanitária Lei 6437/77 - Professor Hércules Riberio', 1, 30.00);  
                          
                        $paymentRequest->setSender(  
                            $nome,  
                            $email,  
                            '',  
                            '',  
                            'CPF',  
                            ''  
                        ); 
                        $paymentRequest->setCurrency("BRL");  

                         // URL para onde o comprador será redirecionado (GET) após o fluxo de pagamento  
                         $paymentRequest->setRedirectUrl("http://webaulavirtual.com.br/aovivo/index.php"); 
                          */  
                                                
                        $sucesso = 'Olá <b>'.$nome.'</b> <br />Seu cadastro foi realizado com sucesso. <br />Para que você possa ter acesso a sala de aula, por favor realize o pagamento.<br />';
                        $_SESSION['cadastro_sucesso'] =  $sucesso;
                        header("Location: ../pagamento.php"); /* Redirect browser */
                        // echo "<script>history.go(-1)</script>"; 
                        
                        
                }
                
                
                
            endif;

    
            
//            if (($qdUser == 1) && ($qdValSenha == 0)):;
//                    $erro = '<strong>Senha incorreta:</strong> a senha que você digitou está incorreta. Tente novamente (tenha certeza de que a função Caps Lock está desligada). <br /><br />Ou entre em contato com a administração relatando o problema através do e-mail:  
//                    <strong>industriadavoz@yahoo.com.br</strong> ou pelo WhatsApp: (61) 9624-7771';
//                            $_SESSION['email'] =  '';
//                            $_SESSION['senha'] =  '';
//                            unset($_SESSION['email']); 
//                            unset($_SESSION['senha']); 
//                            session_destroy();
//                            session_start();
//                            $_SESSION['erro_login'] =  $erro;
//                            echo "<script>history.go(-1)</script>";   
//            endif;


//            if (($qdUser == 1) && ($qdValSenha == 1)):
//                    header("Location: ../saladeaula.php");
//                    $erro = 'Olá, seja bem vindo - Logado com sucesso!';
//            endif;


            if ($qdUser > 1):
                    $erro = 'Entre em contato com a diretoria <strong>Web Aula Virtual</strong>, pois existe 2 usuários com mesmo e-mail cadastrado ';
                            $_SESSION['email'] =  '';
                            $_SESSION['senha'] =  '';
                            unset($_SESSION['email']); 
                            unset($_SESSION['senha']); 
                             unset($_SESSION['cadastro_sucesso']);

                            session_destroy();
                            session_start();
                            $_SESSION['erro_login'] =  $erro;
                            echo "<script>history.go(-1)</script>"; 
            endif;	

            }
    //var_dump($_SESSION);

}//CADASTRO





?>