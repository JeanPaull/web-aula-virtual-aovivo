<?php
    session_start();//INICIA SESSÃO
    if($_SESSION):
            $_SESSION['erro_login'] =  '';
            unset( $_SESSION['erro_login']); 
    endif;
    
    require './includes/configuracoes_site.php';
//    include './includes/funcoes.php';
    
    

    /**********************************************
    //FUNÇÃO CRIAR SESSÃO E VALIDA SESSÃO 
    **********************************************/
    $senha = $_SESSION['senha'];
    $email = $_SESSION['email'];
   
    $query = "SELECT * FROM Cliente WHERE email = '{$email}' AND senha = '{$senha}'";
    $query_pega_user = $conn_pdo->query($query);
    $qdUserSessao = $query_pega_user->rowCount(); 
    if ($qdUserSessao == 0):
            $_SESSION['email'] =  '';
            $_SESSION['senha'] =  '';
            unset($_SESSION['usuario']); 
            unset($_SESSION['senha']); 
            session_destroy();
            header("Location: index.php");
    endif;
    
     if ($qdUserSessao > 1):
            $erro = 'Entre em contato com a diretoria <strong>Web Aula Virtual</strong>, pois existe 2 usuários usando a mesma conta de e-mail.';
            $_SESSION['email'] =  '';
            $_SESSION['senha'] =  '';
            unset($_SESSION['email']); 
            unset($_SESSION['senha']); 
            session_destroy();
            session_start();
            $_SESSION['erro_login'] =  $erro;
            echo "<script>history.go(-1)</script>"; 
    endif;

    if ($qdUserSessao == 1){
        
        while($row = $query_pega_user->fetch()) {
            $nomeUser = $row['nome'];
            $nome  = strtoupper($row['nome']);
            $ativo  = strtoupper($row['ativo']);

        }
        
    }
    else{
        header("Location: index.php");
        echo "<br />Aréa restrita você não está logado<br />";
    }
    /**********************************************
    //FUNÇÃO CRIAR SESSÃO E VALIDA SESSÃO 
    **********************************************/
    
    
    
    
    
//    //Imprimir os resultados
//    while($row = $query_pega_user->fetch()) {
//         echo $row['id']. ' - '. $row['nome'] . ' - ' . $row['email'] . '';
//        
//    }
 ?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Web Aula Virtual - Palestras</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/queries.css">
    <link rel="stylesheet" href="css/etline-font.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style type="text/css">
        .hero .hero-content {
            padding-top: 22%;/*Padding para o vídeo da transmissão*/
        }
        .section-padding {
             padding: 30px 0;
        }
    </style>
    
<!--  
/****************************************************************
* SISTEMA DESENVOLVIPO POR: 
* 
* Jean Paul
* Tel.:  (61) 4063-9660 / (61) 3084-6990 / (61) 84195908
* contato@agenciacriamais.com.br | jeanpaulrodrigues@hotmail.com
* www.agenciacriamais.com.br
* 
*****************************************************************
*/-->
</head>
<body id="top">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <section class="hero">
        <section class="navigation">
            <header>
                <div class="header-content">
                    <div class="logo"><a href="saladeaula.php"><img src="img/sedna-logo.png" alt="Web Aula Virtual logo"></a></div>
                    <div class="header-nav">
                        <nav>
                            <ul class="primary-nav">
                                <li><a href="#institucional">Institucional</a></li>
                                <li><a href="http://webaulavirtual.com.br/" target="new">Cursos</a></li>
                                <li><a href="http://webaulavirtual.com.br/novo/professores/listar" target="new">Professores</a></li>
                                <li><a href="#" target="new">Fale Conosco</a></li>
							  </ul>
                            <ul class="member-actions">
                                <li><a href="#download" class="login">Seja bem vindo(a): <?php echo $nomeUser; ?></a></li>
                                <li><a href="login/sair.php" class="btn-white btn-small">Sair</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                </div>
            </header>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                       
                        <script src="//stream.locaweb.com.br/embed.js"></script>
                        <div id="lws-player" 
                        data-lws-stream="24137088852" data-lws-mode="video" 
                        data-lws-server="//server-1.locaaovivo.com.br/"
                        data-lws-width="840" data-lws-height="460">
                        </div>

                        
                    </div>
                </div>
            </div>
            
            

            
        </div>
<!--        <div class="down-arrow floating-arrow"><a href="#"><i class="fa fa-angle-down"></i></a></div>-->
    </section>
    <section class="intro section-padding" id="institucional">
        <div class="container">
            <div class="row">
<!--                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>SOBRE A EMPRESA</h5>
                        <p>O Portal Webaula Virtual ao longo de sua trajetória, vem contribuindo com a mudança na vida de milhares de pessoas, por meio do conhecimento, oferecendo desde cursos online de atualização, cursos de idiomas e preparatórios para concursos públicos.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>100% online</h5>
                        <p>Ter a flexibilidade de estudar onde quiser e no horário que lhe for mais conveniente, isso não tem preço.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
		                  <span data-icon="&#xe040;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Mais de 100 colaboradores</h5>
                        <p>São mais de 100 colaboradores satisfeitos e integrados trabalhando por uma única missão..</p>
                    </div>
                </div>-->

               
                <h3>TIRE SUAS DÚVIDAS</h3>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div class="fb-comments" data-href="http://agenciacriamais.com.br/palestras_webaulavirtual/saladeaula.php" data-width="100%" data-numposts="5"></div>

            </div>
        </div>
    </section>
    
    <section class="to-top">
        <div class="container">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="footer-links">
                        <ul class="footer-group">
                            <li><a href="http://webaulavirtual.com.br/novo/institucional" target="new">Institucional</a></li>
                            <li><a href="http://webaulavirtual.com.br/novo/comoFunciona" target="new">Como Funciona</a></li>
                            <li><a href="http://webaulavirtual.com.br/novo/professores/listar" target="new">Professores</a></li>
                          <li><a href="#" target="new">Fale Conosco</a></li>
                       
                        </ul>
                        <p>Copyright © 2015 <a href="#"> Web Aula Virtual - Palestras</a><br>
                     Sistema desenvolvido pela   <a href="http://agenciacriamais.com.br"> Agência CriaMais</a></p>
                    </div>
                </div>
                <div class="social-share">
                    <p>Compartilhe com seus amigos</p>
                    <a href="#" class="twitter-share"><i class="fa fa-twitter"></i></a> <a href="#" class="facebook-share"><i class="fa fa-facebook"></i></a>
                </div>
            </div>
        </div>
    </footer>
<!--  
/****************************************************************
* SISTEMA DESENVOLVIPO POR: 
* 
* Jean Paul
* Tel.:  (61) 4063-9660 / (61) 3084-6990 / (61) 84195908
* contato@agenciacriamais.com.br | jeanpaulrodrigues@hotmail.com
* www.agenciacriamais.com.br
* 
*****************************************************************
*/-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/retina.js/dist/retina.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
</body>
</html>