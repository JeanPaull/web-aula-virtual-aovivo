<?php
    session_start();//INICIA SESSÃO
    if($_SESSION):
            $_SESSION['email'] =  '';
            $_SESSION['senha'] =  '';
            unset($_SESSION['email']); 
            unset($_SESSION['senha']); 
            //var_dump($_SESSION);
            unset($_COOKIE);
            //session_destroy();
    endif;

    include_once './includes/configuracoes_site.php';
    //include './includes/funcoes.php';
    //include './login/valida_login.php';
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Web Aula Virtual - Palestras</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/queries.css">
    <link rel="stylesheet" href="css/etline-font.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!--  
/****************************************************************
* SISTEMA DESENVOLVIPO POR: 
* 
* Jean Paul
* Tel.:  (61) 4063-9660 / (61) 3084-6990 / (61) 84195908
* contato@agenciacriamais.com.br | jeanpaulrodrigues@hotmail.com
* www.agenciacriamais.com.br
* 
*****************************************************************
*/-->

<style type="text/css">
    
    .section-padding {
    padding-top: 200px;
}
    
</style>

</head>
<body id="top">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
 
    <section class="navigation" style="background-color: #232731; ">
            <header>
                <div class="header-content">
                    <div class="logo"><a href="index.php"><img src="img/sedna-logo.png" alt="Web Aula Virtual logo"></a></div>
                    <div class="header-nav">
                        <nav>
                            <ul class="primary-nav">
                                <li><a href="index.php">Institucional</a></li>
                                <li><a href="http://webaulavirtual.com.br/" target="new">Cursos</a></li>
                                <li><a href="http://webaulavirtual.com.br/novo/professores/listar" target="new">Professores</a></li>
                                <li><a href="http://webaulavirtual.com.br/novo/atendimento" target="new">Fale Conosco</a></li>
							  </ul>
                            <ul class="member-actions">
<!--                                <li><a href="index.php#download" class="login">Entrar</a></li>-->
                                <li><a href="index.php#download" class="btn-white btn-small">Entrar</a></li>;
                            </ul>
                        </nav>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                </div>
            </header>
        </section>

   
    <section class="sign-up section-padding text-center" id="download" style="background-color:#F3F4F8">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    
                    <?php if(isset($_SESSION['erro_login'])): ?>
                    <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $_SESSION['erro_login'];?>
                    </div>
                    <?php endif; ?>
                    
                    
                    <?php if(isset($_SESSION['cadastro_sucesso'])): ?>
                    <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $_SESSION['cadastro_sucesso'];?>
                    </div>
                   
                    <?php endif; ?>
                    
                    
                    
                    <h3>Cadastre-se, já e comece a estudar na melhor estrutura do Ensino a Distância</h3>
                    <p>Junte-se a mais de 3.000 alunos</p>
                                    
                    <form class="signup-form" action="login/valida_login.php" method="POST" role="form">
                        <div class="form-input-group">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="Entre com Nome" name="nome" required>
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-envelope"></i><input type="text" class="" placeholder="Entre com seu email" name="email" required>
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="Entre com sua senha" name="senha" required>
                        </div>
                        <input type="hidden" name="palestra_aovivo" value="1" />
                        <input type="submit" name="acao" class="btn-fill sign-up-btn" value="Cadastre-se">
                    </form>
                 
                    
                </div>
            </div>
        </div>
    </section>
    <section class="to-top">
        <div class="container">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="footer-links">
                        <ul class="footer-group">
                            <li><a href="http://webaulavirtual.com.br/novo/institucional" target="new">Institucional</a></li>
                            <li><a href="http://webaulavirtual.com.br/novo/comoFunciona" target="new">Como Funciona</a></li>
                            <li><a href="http://webaulavirtual.com.br/novo/professores/listar" target="new">Professores</a></li>
                          <li><a href="#" target="new">Fale Conosco</a></li>
                       
                        </ul>
                        <p>Copyright © 2015 <a href="#"> Web Aula Virtual - Palestras</a><br>
                     Sistema desenvolvido pela   <a href="http://agenciacriamais.com.br"> Agência CriaMais</a></p>
                    </div>
                </div>
                <div class="social-share">
                    <p>Compartilhe com seus amigos</p>
                    <a href="#" class="twitter-share"><i class="fa fa-twitter"></i></a> <a href="#" class="facebook-share"><i class="fa fa-facebook"></i></a>
                </div>
            </div>
        </div>
    </footer>
<!--  
/****************************************************************
* SISTEMA DESENVOLVIPO POR: 
* 
* Jean Paul
* Tel.:  (61) 4063-9660 / (61) 3084-6990 / (61) 84195908
* contato@agenciacriamais.com.br | jeanpaulrodrigues@hotmail.com
* www.agenciacriamais.com.br
* 
*****************************************************************
*/-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/retina.js/dist/retina.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
</body>
</html>